/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_three.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/24 16:16:58 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/11 15:15:09 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	ft_sort(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
	t_env *env)
{
	int		first;
	int		second;
	int		last;

	first = *((int*)(*dlist_ba)->first->content);
	second = *((int*)(*dlist_ba)->first->next->content);
	last = *((int*)(*dlist_ba)->last->content);
	if (first < second && second > last && last > first)
	{
		ft_apply_instructions("sa", env, dlist_ba, dlist_bb);
		ft_apply_instructions("ra", env, dlist_ba, dlist_bb);
	}
	if (first > second && second < last && last > first)
		ft_apply_instructions("sa", env, dlist_ba, dlist_bb);
	if (first < second && second > last && last < first)
		ft_apply_instructions("rra", env, dlist_ba, dlist_bb);
	if (first > second && second < last && last < first)
		ft_apply_instructions("ra", env, dlist_ba, dlist_bb);
	if (first > second && second > last && last < first)
	{
		ft_apply_instructions("sa", env, dlist_ba, dlist_bb);
		ft_apply_instructions("rra", env, dlist_ba, dlist_bb);
	}
}

void		ft_sort_three(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
	t_env *env)
{
	if (ft_check_if_sorted(*dlist_ba, *dlist_bb))
		return ;
	if ((*dlist_ba)->size == 2)
		ft_apply_instructions("sa", env, dlist_ba, dlist_bb);
	else if ((*dlist_ba)->size == 3)
		ft_sort(dlist_ba, dlist_bb, env);
}
