/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   split_lists.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/24 13:38:52 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/27 21:51:53 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	ft_exists_pivot_sup(t_dlist_b *dlist_b, int pivot)
{
	t_dlist	*elem;

	elem = dlist_b->first;
	while (elem != dlist_b->last)
	{
		if (*((int*)(elem->content)) > pivot)
			return (1);
		elem = elem->next;
	}
	if (elem == dlist_b->last && *((int*)(elem->content)) > pivot)
		return (1);
	return (0);
}

static int	ft_exists_pivot_inf(t_dlist_b *dlist_b, int pivot)
{
	t_dlist	*elem;

	elem = dlist_b->first;
	while (elem != dlist_b->last)
	{
		if (*((int*)(elem->content)) <= pivot)
			return (1);
		elem = elem->next;
	}
	if (elem == dlist_b->last && *((int*)(elem->content)) <= pivot)
		return (1);
	return (0);
}

void		ft_split_sup_b(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
		t_env *env, int pivot)
{
	t_dlist	*listb;

	while (ft_exists_pivot_sup(*dlist_bb, pivot))
	{
		listb = (*dlist_bb)->first;
		if (*((int*)(listb->content)) > pivot)
			ft_apply_instructions("pa", env, dlist_ba, dlist_bb);
		else if (ft_is_worth_to_rotate_sup(*dlist_bb, pivot))
			ft_apply_instructions("rb", env, dlist_ba, dlist_bb);
		else
			ft_apply_instructions("rrb", env, dlist_ba, dlist_bb);
	}
}

void		ft_split_sup_a(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
		t_env *env, size_t len)
{
	t_dlist	*lista;
	int		pivot;

	pivot = ft_get_median(*dlist_ba, len);
	while (ft_exists_pivot_sup(*dlist_ba, pivot))
	{
		lista = (*dlist_ba)->first;
		if (*((int*)(lista->content)) > pivot)
			ft_apply_instructions("pb", env, dlist_ba, dlist_bb);
		else if (ft_is_worth_to_rotate_sup(*dlist_ba, pivot))
			ft_apply_instructions("ra", env, dlist_ba, dlist_bb);
		else
			ft_apply_instructions("rra", env, dlist_ba, dlist_bb);
	}
}

void		ft_split_inf_a(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
		t_env *env, size_t len)
{
	t_dlist	*lista;
	int		pivot;
	int		ra;
	size_t	size;

	ra = 0;
	size = (*dlist_ba)->size;
	pivot = ft_get_median(*dlist_ba, len);
	while (ft_exists_pivot_inf(*dlist_ba, pivot))
	{
		lista = (*dlist_ba)->first;
		if (*((int*)(lista->content)) <= pivot)
			ft_apply_instructions("pb", env, dlist_ba, dlist_bb);
		else if (len < size || ft_is_worth_to_rotate_inf(*dlist_ba, pivot))
		{
			ft_apply_instructions("ra", env, dlist_ba, dlist_bb);
			ra++;
		}
		else if (len == size)
			ft_apply_instructions("rra", env, dlist_ba, dlist_bb);
	}
	while (len < size && ra > 0)
	{
		ft_apply_instructions("rra", env, dlist_ba, dlist_bb);
		ra--;
	}
}
