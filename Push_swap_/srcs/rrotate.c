/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rrotate.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/11 19:22:26 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/11 16:30:08 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_rrotate(t_dlist_b **dlist_b)
{
	t_dlist	*new_first;
	t_dlist	*new_last;

	if (!(*dlist_b)->first || (*dlist_b)->size == 1)
		return ;
	new_first = (*dlist_b)->first->prev;
	new_last = (*dlist_b)->last->prev;
	(*dlist_b)->first = new_first;
	(*dlist_b)->last = new_last;
}
