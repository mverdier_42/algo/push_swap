/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/11 19:23:35 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/11 16:29:40 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	ft_push_to_des(t_dlist_b **des, t_dlist **tomove)
{
	if ((*des)->size == 1)
	{
		(*tomove)->next = (*des)->first;
		(*tomove)->prev = (*des)->last;
		(*des)->first->prev = *tomove;
	}
	else if ((*des)->size > 0)
	{
		(*tomove)->next = (*des)->first;
		(*tomove)->prev = (*des)->last;
		(*des)->first->prev = *tomove;
	}
	else
	{
		(*tomove)->next = *tomove;
		(*tomove)->prev = *tomove;
		(*des)->last = *tomove;
	}
	(*des)->first = *tomove;
	(*des)->last->next = (*des)->first;
}

static void	ft_push_from_one_src(t_dlist_b **src, t_dlist_b **des)
{
	t_dlist	*tomove;

	tomove = (*src)->first;
	(*src)->first = NULL;
	(*src)->last = NULL;
	ft_push_to_des(des, &tomove);
}

static void	ft_push_from_multiple_src(t_dlist_b **src, t_dlist_b **des)
{
	t_dlist	*tomove;

	tomove = (*src)->first;
	(*src)->first = ((*src)->first)->next;
	(*src)->first->prev = (*src)->last;
	(*src)->last->next = (*src)->first;
	ft_push_to_des(des, &tomove);
}

void		ft_push(t_dlist_b **src, t_dlist_b **des)
{
	if (!(*src) || (*src)->size == 0)
		return ;
	if ((*src)->size == 1)
		ft_push_from_one_src(src, des);
	else
		ft_push_from_multiple_src(src, des);
	(*src)->size--;
	(*des)->size++;
}
