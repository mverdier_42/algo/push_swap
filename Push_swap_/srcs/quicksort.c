/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quicksort.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/16 15:15:50 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/27 21:24:45 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

/*static void	ft_quicksort_b(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
		t_env *env)
{
	size_t	new_size;
	size_t	i;
	int		pivot;

	new_size = (*dlist_bb)->size;
	while (new_size > 0)
	{
		if ((*dlist_bb)->size > 5)
		{
			pivot = ft_get_five_biggers(*dlist_bb);
			ft_split_sup_b(dlist_ba, dlist_bb, env, pivot);
		}
		if ((*dlist_bb)->size <= 5)
		{
			new_size -= (*dlist_bb)->size;
			ft_sort_b_five(dlist_ba, dlist_bb, env);
			i = 0;
			while (i < 5 && i < new_size)
			{
				ft_apply_instructions("pb", env, dlist_ba, dlist_bb);
				i++;
			}
		}
	}
}*/

static void	ft_push_last(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb, t_env *env,
		size_t len)
{
	size_t	i;

	i = 0;
	while (i < len)
	{
		ft_apply_instructions("pb", env, dlist_ba, dlist_bb);
		i++;
	}
}

static void	ft_quicksort_b(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
		t_env *env)
{
	int		pivot;
	size_t	old_size;
	size_t	new_size;

	old_size = 0;
	new_size = 0;
	if ((*dlist_bb)->size > 5)
	{
		pivot = ft_get_median(*dlist_bb, (*dlist_bb)->size);
		old_size = (*dlist_bb)->size;
		ft_split_sup_b(dlist_ba, dlist_bb, env, pivot);
		new_size = (*dlist_bb)->size;
		if ((*dlist_bb)->size > 5)
			ft_quicksort_b(dlist_ba, dlist_bb, env);
	}
	ft_sort_b_five(dlist_ba, dlist_bb, env);
	if (old_size > 0 && new_size > 0 && old_size - new_size > 5)
	{
		ft_split_inf_a(dlist_ba, dlist_bb, env, old_size - new_size);
		ft_quicksort_b(dlist_ba, dlist_bb, env);
	}
	else if (old_size > 0 && new_size > 0 && old_size - new_size <= 5)
	{
//		ft_printf("size = %u\n", old_size - new_size);
		ft_push_last(dlist_ba, dlist_bb, env, old_size - new_size);
		ft_quicksort_b(dlist_ba, dlist_bb, env);
	}
}

int			ft_quicksort(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb, t_env *env)
{
	if (ft_check_if_sorted(*dlist_ba, *dlist_bb))
		return (1);
	ft_split_inf_a(dlist_ba, dlist_bb, env, (*dlist_ba)->size);
	ft_quicksort_b(dlist_ba, dlist_bb, env);
	ft_split_sup_a(dlist_ba, dlist_bb, env, (*dlist_ba)->size);
	ft_quicksort_b(dlist_ba, dlist_bb, env);
	return (1);
}
