/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/11 12:47:19 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/10 15:13:16 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include "../libft.h"

# include <sys/types.h>
# include <sys/uio.h>
# include <unistd.h>
# include <stdlib.h>

# define BUFF_SIZE 8

typedef struct	s_buf
{
	char			buf[BUFF_SIZE];
	int				fd;
	int				nb;
	int				line_len;
}				t_buf;

int				get_next_line(const int fd, char **line);

#endif
