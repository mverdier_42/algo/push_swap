/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insert_elem.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 18:47:10 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/10 16:59:21 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	ft_get_elem(t_dlist_b *dlist_bb, size_t min_pos)
{
	t_dlist	*elem;

	elem = dlist_bb->first;
	while (min_pos > 0)
	{
		elem = elem->next;
		min_pos--;
	}
	return (*((int*)elem->content));
}

static void	ft_apply_left_half(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
	t_env *env, int elem)
{
	size_t	forward;
	size_t	backward;
	size_t	pos;
	t_dlist	*tmp;

	tmp = (*dlist_bb)->first;
	pos = 0;
	while (tmp && *((int*)tmp->content) != elem)
	{
		tmp = tmp->next;
		pos++;
	}
	forward = ft_count_forward_a(*dlist_ba, elem);
	backward = ft_count_backward_a(*dlist_ba, elem);
	if (forward <= backward && forward > 0)
		ft_apply_instructions("rr", env, dlist_ba, dlist_bb);
	else
		ft_apply_instructions("rb", env, dlist_ba, dlist_bb);
	if (backward < forward && backward > 0)
		ft_apply_instructions("rra", env, dlist_ba, dlist_bb);
}

static void	ft_apply_right_half(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
	t_env *env, int elem)
{
	size_t	forward;
	size_t	backward;
	size_t	pos;
	t_dlist	*tmp;

	tmp = (*dlist_bb)->first;
	pos = 0;
	while (tmp && *((int*)tmp->content) != elem)
	{
		tmp = tmp->next;
		pos++;
	}
	forward = ft_count_forward_a(*dlist_ba, elem);
	backward = ft_count_backward_a(*dlist_ba, elem);
	if (backward <= forward && backward > 0)
		ft_apply_instructions("rrr", env, dlist_ba, dlist_bb);
	else
		ft_apply_instructions("rrb", env, dlist_ba, dlist_bb);
	if (forward < backward && forward > 0)
		ft_apply_instructions("ra", env, dlist_ba, dlist_bb);
}

static void	ft_rotate_a(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb, t_env *env,
	int elem)
{
	size_t	forward;
	size_t	backward;

	forward = ft_count_forward_a(*dlist_ba, elem);
	backward = ft_count_backward_a(*dlist_ba, elem);
	while (forward > 0 || backward > 0)
	{
		if (forward > 0 && forward <= backward)
			ft_apply_instructions("ra", env, dlist_ba, dlist_bb);
		else if (backward > 0 && backward < forward)
			ft_apply_instructions("rra", env, dlist_ba, dlist_bb);
		forward = ft_count_forward_a(*dlist_ba, elem);
		backward = ft_count_backward_a(*dlist_ba, elem);
	}
}

void		ft_insert_elem(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
	t_env *env, size_t min_pos)
{
	size_t	pos_tmp;
	int		elem;

	pos_tmp = min_pos;
	elem = ft_get_elem(*dlist_bb, min_pos);
	while (pos_tmp > 0 && pos_tmp < (*dlist_bb)->size)
	{
		if (min_pos <= (*dlist_bb)->size / 2)
		{
			ft_apply_left_half(dlist_ba, dlist_bb, env, elem);
			pos_tmp--;
		}
		else if (min_pos > (*dlist_bb)->size / 2)
		{
			ft_apply_right_half(dlist_ba, dlist_bb, env, elem);
			pos_tmp++;
		}
	}
	ft_rotate_a(dlist_ba, dlist_bb, env, elem);
	ft_apply_instructions("pa", env, dlist_ba, dlist_bb);
}
