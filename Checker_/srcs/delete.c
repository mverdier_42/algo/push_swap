/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   delete.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/11 13:20:32 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/11 17:05:03 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void	ft_delete_lists(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb, t_env **env,
		t_dlist_b **args)
{
	ft_dlstdel(dlist_ba, &free);
	ft_dlstdel(dlist_bb, &free);
	ft_dlstdel(args, &free);
	free(*dlist_ba);
	free(*dlist_bb);
	free(*args);
	free(*env);
	dlist_ba = NULL;
	dlist_bb = NULL;
	args = NULL;
	env = NULL;
}
