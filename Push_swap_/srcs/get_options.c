/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_options.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/13 16:53:22 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/11 15:43:07 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	ft_existing_option(char c)
{
	if (c == 'v')
		ft_printf("Error : the option [-v] is already used !\n");
	else if (c == 'f')
		ft_printf("Error : the option [-f] is already used !\n");
	return (0);
}

static int	ft_get_v(char *str, t_env **env)
{
	if (!ft_strcmp(str, "v") || !ft_strcmp(str, "vf")
			|| !ft_strcmp(str, "fv"))
	{
		if ((*env)->v)
			return (ft_existing_option('v'));
		(*env)->v = 1;
	}
	else
		return (2);
	return (1);
}

static int	ft_get_f(char *str, t_env **env)
{
	if (!ft_strcmp(str, "f") || !ft_strcmp(str, "fv")
			|| !ft_strcmp(str, "vf"))
	{
		if ((*env)->f)
			return (ft_existing_option('f'));
		(*env)->f = 1;
	}
	else
		return (2);
	return (1);
}

int			ft_get_options(t_dlist **args, t_dlist *last, t_env **env,
		size_t *n)
{
	int		ret1;
	int		ret2;
	char	*tmp;

	while ((*args) && (tmp = (char*)((*args)->content))[0] == '-')
	{
		if (!(ret1 = ft_get_v(tmp + 1, env))
				|| !(ret2 = ft_get_f(tmp + 1, env)))
			return (0);
		if (ret1 == 1 || ret2 == 1)
			(*n)++;
		if (ret1 == 2 && ret2 == 2 && !ft_str_isdigit(tmp + 1))
		{
			ft_printf("Error : the command [%s] is not known !\n", tmp);
			return (0);
		}
		else if ((ret1 == 2 && ret2 == 2) || (*args) == last)
			break ;
		(*args) = (*args)->next;
	}
	return (1);
}
