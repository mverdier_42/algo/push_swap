/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_if_sorted.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/10 19:08:16 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/11 15:32:34 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static int	ft_b_is_empty(t_dlist_b *dlist_bb)
{
	if (dlist_bb->size != 0)
		return (0);
	return (1);
}

static int	ft_check_if_a_sorted(t_dlist_b *dlist_ba)
{
	t_dlist	*tmp;
	t_dlist	*temp;

	tmp = dlist_ba->first;
	temp = tmp->next;
	while (temp)
	{
		if (*((int*)(tmp->content)) > *((int*)(temp->content)))
			return (0);
		if (temp == dlist_ba->last)
			break ;
		tmp = temp;
		temp = temp->next;
	}
	return (1);
}

int			ft_check_if_sorted(t_dlist_b *dlist_ba, t_dlist_b *dlist_bb,
		t_env *env)
{
	if (!ft_b_is_empty(dlist_bb))
	{
		if (env->v)
			ft_printf("KO : the list B is not empty !\n");
		return (0);
	}
	if (!ft_check_if_a_sorted(dlist_ba))
	{
		if (env->v)
			ft_printf("KO : the list A is not sorted !\n");
		return (0);
	}
	return (1);
}
