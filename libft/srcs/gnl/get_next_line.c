/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/14 10:44:06 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/10 16:04:05 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

static char	*buf_transfer_n(t_buf *lst, char **line)
{
	int i;

	i = 0;
	while (i < lst->nb && lst->buf[i] != '\n')
		i++;
	if ((*line = ft_nrealloc(*line, (lst->line_len + i) + 1,
					lst->line_len)) == NULL)
		return (NULL);
	ft_memcpy((*line + lst->line_len), lst->buf, i);
	(*line)[lst->line_len + i] = '\0';
	lst->line_len += i;
	ft_memmove(lst->buf, ft_memchr(lst->buf, '\n', lst->nb) + 1,
			BUFF_SIZE - (i + 1));
	lst->nb -= (i + 1);
	return (*line);
}

static char	*buf_transfer_nb(t_buf *lst, char **line)
{
	if ((*line = ft_nrealloc(*line, (lst->line_len + lst->nb) + 1,
					lst->line_len)) == NULL)
		return (NULL);
	ft_memcpy((*line + lst->line_len), lst->buf, lst->nb);
	(*line)[lst->line_len + lst->nb] = '\0';
	lst->line_len += lst->nb;
	return (*line);
}

static int	get_next_line_r(t_buf *lst, char **line)
{
	int			tmp;

	if (!lst)
		return (-1);
	if ((ft_memchr(lst->buf, '\n', lst->nb)))
	{
		if ((*line = buf_transfer_n(lst, line)) == NULL)
			return (-1);
		lst->line_len = 0;
		return (1);
	}
	if ((*line = buf_transfer_nb(lst, line)) == NULL)
		return (-1);
	tmp = lst->nb;
	lst->nb = read(lst->fd, lst->buf, BUFF_SIZE);
	if (tmp == 0 && lst->nb == 0)
		return (0);
	else if (lst->nb < 0)
		return (-1);
	else if (lst->nb > 0)
		get_next_line_r(lst, line);
	lst->line_len = 0;
	return (1);
}

static void	push_next_lst(const int fd, t_list **list)
{
	t_buf	*lst;

	if ((lst = (t_buf*)malloc(sizeof(t_buf))) == NULL)
		return ;
	lst->fd = fd;
	lst->nb = 0;
	lst->line_len = 0;
	ft_lst_push_back(list, ft_lstnew(lst));
}

int			get_next_line(const int fd, char **line)
{
	static t_list	*list = NULL;
	t_buf			*lst;
	t_list			*tmp;
	int				ret;

	tmp = list;
	while (tmp && fd != ((t_buf*)(tmp->content))->fd)
		tmp = tmp->next;
	if (!tmp || fd != ((t_buf*)(tmp->content))->fd)
		push_next_lst(fd, &list);
	tmp = list;
	while (tmp && fd != ((t_buf*)(tmp->content))->fd)
		tmp = tmp->next;
	lst = tmp->content;
	if (lst->nb < 0)
		lst->nb = 0;
	if (fd < 0 || line == NULL)
		return (-1);
	*line = NULL;
	if ((ret = get_next_line_r(tmp->content, line)) < 1)
		ft_lstdelone(&list, &free);
	return (ret);
}
