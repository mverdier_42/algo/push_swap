/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 15:41:50 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/10 16:20:42 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKER_H
# define CHECKER_H

# include "libft.h"

typedef struct		s_env
{
	int				a;
	int				v;
	int				f;
	int				fd;
	int				speed;
}					t_env;

void				*ft_init_lists(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
					t_dlist_b **args);
t_env				*ft_init_env(void);
int					ft_parse(t_dlist_b *args, t_env **env);
int					ft_get_options(t_dlist **args, t_dlist *last, t_env **env,
					size_t *n);
int					ft_get_fd(char *str, t_env **env);
int					ft_insert(t_dlist_b **dlist_ba, t_dlist_b *args,
					t_env *env, int ret);

int					ft_execute_instructions(t_dlist_b **dlist_ba,
					t_dlist_b **dlist_bb, t_env *env);
int					ft_check_if_sorted(t_dlist_b *dlist_ba,
					t_dlist_b *dlist_bb, t_env *env);
void				ft_aff(t_dlist_b *dlist_ba, t_dlist_b *dlist_bb,
					char *line);

void				ft_push(t_dlist_b **src, t_dlist_b **des);
void				ft_swap(t_dlist_b **dlist_b);
void				ft_ss(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb);
void				ft_rotate(t_dlist_b **dlist_b);
void				ft_rr(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb);
void				ft_rrotate(t_dlist_b **dlist_b);
void				ft_rrr(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb);

void				ft_delete_lists(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
					t_env **env, t_dlist_b **args);

#endif
