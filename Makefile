# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mverdier <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/10/09 13:44:17 by mverdier          #+#    #+#              #
#    Updated: 2017/10/13 18:08:38 by mverdier         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

# Colors.

RED =		\033[1;31m

RES =		\033[0m

#------------------------------------------------------------------------------#
# Primary rules

all: libft/libft checker/checker push_swap/push_swap

libft/libft:
	@$(MAKE) -C ./libft/

checker/checker:
	@$(MAKE) -C ./Checker_/

push_swap/push_swap:
	@$(MAKE) -C ./Push_swap_/

clean:
	@$(MAKE) -C ./libft/ clean
	@$(MAKE) -C ./Checker_/ clean
	@$(MAKE) -C ./Push_swap_/ clean

fclean:
	@$(MAKE) -C ./libft/ fclean
	@$(MAKE) -C ./Checker_/ fclean
	@$(MAKE) -C ./Push_swap_/ fclean

re: fclean
	@$(MAKE) all

#------------------------------------------------------------------------------#
# List of all my optionnals but usefull rules.

no:
	@$(MAKE) -C ./libft/ no
	@$(MAKE) -C ./Checker_/ no
	@$(MAKE) -C ./Push_swap_/ no

check: no

git:	# A rule to make git add easier
	@$(MAKE) -C ./libft/ git
	@$(MAKE) -C ./Checker_/ git
	@$(MAKE) -C ./Push_swap_/ git
	@git add auteur .gitignore Makefile
	@git status

.PHONY: all clean re fclean git no printf check

cleantmp:
	@printf "$(RED)"
	@rm -rf ./Checker_/.*				\
			./Checker_/srcs/.*			\
			./Checker_/includes/.*		\
			./Push_swap_/.*				\
			./Push_swap_/srcs/.*		\
			./Push_swap_/includes/.*	\
			./libft/.*					\
			./libft/srcs/libft/.*		\
			./libft/includes/.*			\
			./libft/includes/libft/.*
	@printf "$(RES)"
