/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insert.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 19:15:28 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/11 17:41:46 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	check_duplicates(t_dlist_b *dlist_b, int nb)
{
	t_dlist	*list;

	list = dlist_b->first;
	while (list && list != dlist_b->last)
	{
		if (nb == *((int*)(list->content)))
			return (0);
		list = list->next;
	}
	if (list && list == dlist_b->last)
	{
		if (nb == *((int*)(list->content)))
			return (0);
	}
	return (1);
}

static int	ft_check_insert(t_dlist_b **dlist_b, char *str, t_env *env)
{
	int		*nb;

	if ((nb = (int*)malloc(sizeof(int))) == NULL)
		return (0);
	*nb = ft_atoi(str);
	if (!check_duplicates(*dlist_b, *nb))
	{
		if (env->v)
			ft_printf("Error : the argument [%s] is a duplicate !\n", str);
		free(nb);
		return (0);
	}
	ft_dlst_push_back(dlist_b, ft_dlstnew(nb));
	return (1);
}

static int	ft_check_for_all(t_dlist_b *args, t_env *env,
		t_dlist_b **dlist_b, t_dlist *last)
{
	t_dlist	*elem;

	elem = args->first;
	while (elem)
	{
		if (!ft_check_insert(dlist_b, elem->content, env))
			return (0);
		if (elem == last)
			break ;
		elem = elem->next;
	}
	return (1);
}

int			ft_insert(t_dlist_b **dlist_b, t_dlist_b *args, t_env *env)
{
	char		*tmp;
	t_dlist_b	*ins_args;
	t_dlist		*last;
	size_t		i;

	i = 0;
	last = args->last;
	ins_args = args;
	while (ins_args->first && (tmp = (char*)ins_args->first->content) &&
			(!ft_strcmp(tmp, "-v") || !ft_strcmp(tmp, "-f")
			|| !ft_strcmp(tmp, "-vf") || !ft_strcmp(tmp, "-fv")))
	{
		i++;
		ft_rotate(&ins_args);
	}
	if (args->size == 0 || (env->v && !env->f && args->size < 1)
			|| (env->f && !env->v && args->size < 2)
			|| (env->v && env->f && args->size < 1 + i))
		return (2);
	if (env->f)
		ft_rotate(&ins_args);
	if (!ft_check_for_all(ins_args, env, dlist_b, last))
		return (0);
	return (1);
}
