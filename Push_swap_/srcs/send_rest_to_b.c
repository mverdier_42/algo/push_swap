/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   send_rest_to_b.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 15:14:07 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/05 17:40:13 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_send_rest_to_b(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
		t_env *env, t_serie serie)
{
	t_dlist	*elem;

	elem = (*dlist_ba)->first;
	if (*((int*)elem->content) > serie.start
			&& *((int*)elem->content) <= serie.end)
	{
		while (*((int*)elem->content) > serie.start
			&& *((int*)elem->content) <= serie.end)
		{
			ft_apply_instructions("ra", env, dlist_ba, dlist_bb);
			elem = (*dlist_ba)->first;
		}
	}
	while (*((int*)elem->prev->content) != serie.end)
	{
		ft_apply_instructions("rra", env, dlist_ba, dlist_bb);
		elem = (*dlist_ba)->first;
	}
	while (*((int*)elem->content) != serie.start)
	{
		ft_apply_instructions("pb", env, dlist_ba, dlist_bb);
		elem = (*dlist_ba)->first;
	}
}
