/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_algo.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 17:36:10 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/10 21:47:17 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		ft_sort_algo(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb, t_env *env)
{
	if (ft_check_if_sorted(*dlist_ba, *dlist_bb))
		return (1);
	while ((*dlist_ba)->size > 2)
		ft_apply_instructions("pb", env, dlist_ba, dlist_bb);
	if (!ft_insert_in_a(dlist_ba, dlist_bb, env))
		return (0);
	return (1);
}
