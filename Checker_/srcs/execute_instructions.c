/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execute_instructions.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/10 17:50:01 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/11 15:31:57 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"
#include <fcntl.h>

static int	ft_execute_a(t_dlist_b **dlist_ba, char *line)
{
	if (!ft_strcmp(line, "sa"))
		ft_swap(dlist_ba);
	else if (!ft_strcmp(line, "ra"))
		ft_rotate(dlist_ba);
	else if (!ft_strcmp(line, "rra"))
		ft_rrotate(dlist_ba);
	else
		return (0);
	return (1);
}

static int	ft_execute_b(t_dlist_b **dlist_bb, char *line)
{
	if (!ft_strcmp(line, "sb"))
		ft_swap(dlist_bb);
	else if (!ft_strcmp(line, "rb"))
		ft_rotate(dlist_bb);
	else if (!ft_strcmp(line, "rrb"))
		ft_rrotate(dlist_bb);
	else
		return (0);
	return (1);
}

static int	ft_execute_both(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
		char *line)
{
	if (!ft_strcmp(line, "ss"))
		ft_ss(dlist_ba, dlist_bb);
	else if (!ft_strcmp(line, "pa"))
		ft_push(dlist_bb, dlist_ba);
	else if (!ft_strcmp(line, "pb"))
		ft_push(dlist_ba, dlist_bb);
	else if (!ft_strcmp(line, "rr"))
		ft_rr(dlist_ba, dlist_bb);
	else if (!ft_strcmp(line, "rrr"))
		ft_rrr(dlist_ba, dlist_bb);
	else
		return (0);
	return (1);
}

static void	ft_exec_visual(t_env *env)
{
	char	*line;

	if (env->speed == 0)
	{
		get_next_line(0, &line);
		free(line);
	}
	else
		usleep(500000);
}

int			ft_execute_instructions(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
		t_env *env)
{
	char	*line;

	if (env->a)
		ft_aff(*dlist_ba, *dlist_bb, NULL);
	while (get_next_line(env->fd, &line) > 0)
	{
		if (env->a)
			ft_exec_visual(env);
		if (!ft_execute_a(dlist_ba, line) && !ft_execute_b(dlist_bb, line) &&
				!ft_execute_both(dlist_ba, dlist_bb, line))
		{
			if (env->v)
				ft_printf("Error : the instruction \"%s\" is not known !\n",
				line);
			free(line);
			return (0);
		}
		if (env->a)
			ft_aff(*dlist_ba, *dlist_bb, line);
		free(line);
	}
	free(line);
	return (1);
}
