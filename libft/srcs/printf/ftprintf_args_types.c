/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ftprintf_args_types.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/14 15:58:07 by mverdier          #+#    #+#             */
/*   Updated: 2016/12/14 15:58:15 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ftprintf_args.h"

int		arg_is_positive(t_args *arg)
{
	if (arg->type == UINT || arg->type == ULINT || arg->type == ULLINT
			|| arg->type == SIZE_T || arg->type == UINTMAX_T)
		return (1);
	else if (arg->type == CHAR || arg->type == STR || arg->type == WINT_T
			|| arg->type == WCHAR_T)
		return (2);
	else if (arg->arg.i >= 0)
		return (1);
	return (0);
}

int		arg_is_zero(t_args *arg)
{
	if (arg_is_uint(arg) && arg->arg.ui == 0)
		return (1);
	else if (arg_is_char(arg))
		return (2);
	else if (arg->arg.i == 0)
		return (1);
	return (0);
}

int		arg_is_int(t_args *arg)
{
	if (arg->type == INT || arg->type == LINT || arg->type == LLINT
			|| arg->type == INTMAX_T || arg->type == SSIZE_T)
		return (1);
	return (0);
}

int		arg_is_uint(t_args *arg)
{
	if (arg->type == UINT || arg->type == ULINT || arg->type == ULLINT
			|| arg->type == UINTMAX_T || arg->type == SIZE_T)
		return (1);
	return (0);
}

int		arg_is_char(t_args *arg)
{
	if (arg->type == CHAR || arg->type == STR || arg->type == WCHAR_T
			|| arg->type == WINT_T)
		return (1);
	return (0);
}
