/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parser.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 15:40:33 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/11 15:43:51 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	ft_check_max_int(char *str)
{
	int		i;
	int		sign;
	int		len;

	i = 0;
	sign = 1;
	if (str[i] == '-' || str[i] == '+')
	{
		if (str[i] == '-')
			sign = -1;
		i++;
	}
	len = ft_strlen(str + i);
	if (len < 10 || (len == 10 && str[i] < '2'))
		return (1);
	if (len > 10 || (len == 10 && str[i] > '2'))
		return (0);
	if (len == 10 && str[i] == '2')
	{
		if (sign > 0 && ft_atoi(str + i + 1) > 147483647)
			return (0);
		if (sign < 0 && ft_atoi(str + i + 1) > 147483648)
			return (0);
	}
	return (1);
}

static int	ft_check_int(char *str, char *tmp, t_env *env)
{
	if (!ft_str_isdigit(tmp))
	{
		if (env->v)
			ft_printf("Error : the argument [%s] is not an int !\n", str);
		return (0);
	}
	if (!ft_check_max_int(str))
	{
		if (env->v)
			ft_printf("Error : the argument [%s] is too big for an int !\n",
			str);
		return (0);
	}
	return (1);
}

static int	ft_check(t_dlist *args, t_dlist *last, t_env *env)
{
	char	*tmp;
	t_dlist	*elem;

	elem = args;
	while (elem)
	{
		tmp = (char*)elem->content;
		if (tmp[0] == '-' || tmp[0] == '+')
			tmp++;
		if (!ft_check_int(elem->content, tmp, env))
			return (0);
		if (elem == last)
			break ;
		elem = elem->next;
	}
	return (1);
}

int			ft_parse(t_dlist_b *args, t_env **env)
{
	t_dlist		*parse_args;
	size_t		i;

	parse_args = args->first;
	i = 0;
	if (!ft_get_options(&parse_args, args->last, env, &i))
		return (0);
	else if (args->size == 0 || ((*env)->v && !(*env)->f && args->size <= 1)
			|| ((*env)->f && !(*env)->v && args->size <= 2)
			|| ((*env)->f && (*env)->v && args->size <= 1 + i))
		return (2);
	if ((*env)->f && !ft_get_fd(parse_args->content, env))
		return (0);
	if ((*env)->fd != 1)
		parse_args = parse_args->next;
	if (!ft_check(parse_args, args->last, *env))
		return (0);
	return (1);
}
