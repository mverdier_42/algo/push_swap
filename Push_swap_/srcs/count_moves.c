/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   count_moves.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 20:10:07 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/06 16:06:22 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

size_t	ft_count_forward_a(t_dlist_b *dlist_ba, int elem)
{
	t_dlist	*tmp;
	size_t	forward;

	forward = 0;
	tmp = dlist_ba->first;
	while (tmp)
	{
		if ((elem > *((int*)tmp->prev->content) && (elem < *((int*)tmp->content)
			|| *((int*)tmp->content) < *((int*)tmp->prev->content)))
			|| (elem < *((int*)tmp->content)
				&& (elem > *((int*)tmp->prev->content)
			|| *((int*)tmp->content) < *((int*)tmp->prev->content))))
			break ;
		if (tmp == dlist_ba->last)
			break ;
		tmp = tmp->next;
		forward++;
	}
	return (forward);
}

size_t	ft_count_backward_a(t_dlist_b *dlist_ba, int elem)
{
	t_dlist	*tmp;
	size_t	backward;

	backward = 0;
	tmp = dlist_ba->first;
	while (tmp)
	{
		if ((elem > *((int*)tmp->prev->content) && (elem < *((int*)tmp->content)
			|| *((int*)tmp->content) < *((int*)tmp->prev->content)))
			|| (elem < *((int*)tmp->content)
				&& (elem > *((int*)tmp->prev->content)
			|| *((int*)tmp->content) < *((int*)tmp->prev->content))))
			break ;
		if (tmp->prev == dlist_ba->first)
			break ;
		tmp = tmp->prev;
		backward++;
	}
	return (backward);
}

size_t	ft_count_forward(t_dlist_b *dlist_ba, int elem)
{
	t_dlist	*tmp;
	size_t	forward;

	forward = 0;
	tmp = dlist_ba->first;
	while (tmp)
	{
		if (*((int*)tmp->content) == elem)
			break ;
		if (tmp == dlist_ba->last)
			break ;
		tmp = tmp->next;
		forward++;
	}
	return (forward);
}

size_t	ft_count_backward(t_dlist_b *dlist_ba, int elem)
{
	t_dlist	*tmp;
	size_t	backward;

	backward = 0;
	tmp = dlist_ba->first;
	while (tmp)
	{
		if (*((int*)tmp->content) == elem)
			break ;
		if (tmp->prev == dlist_ba->first)
			break ;
		tmp = tmp->prev;
		backward++;
	}
	return (backward);
}
