/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insert_in_a.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 15:22:41 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/06 19:12:19 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static t_list	*ft_count_all_instructions(t_dlist_b *dlist_ba,
	t_dlist_b *dlist_bb)
{
	t_list	*count;
	t_dlist	*elem;
	size_t	*nbr;
	size_t	pos;

	count = NULL;
	pos = 0;
	elem = dlist_bb->first;
	while (elem)
	{
		if ((nbr = (size_t*)malloc(sizeof(size_t))) == NULL)
		{
			ft_lstdel(&count, &free);
			return (NULL);
		}
		*nbr = ft_count_instructions(dlist_ba, dlist_bb, elem, pos);
		ft_lst_push_back(&count, ft_lstnew(nbr));
		if (elem == dlist_bb->last)
			break ;
		elem = elem->next;
		pos++;
	}
	return (count);
}

static size_t	ft_get_min_pos(t_list *count)
{
	t_list	*tmp;
	size_t	pos;
	size_t	i;
	int		min;

	pos = 0;
	i = 0;
	tmp = count;
	min = *((int*)tmp->content);
	while (tmp)
	{
		if (*((int*)tmp->content) < min)
		{
			min = *((int*)tmp->content);
			pos = i;
		}
		i++;
		tmp = tmp->next;
	}
	return (pos);
}

static int		ft_get_min(t_dlist_b *dlist_ba)
{
	t_dlist	*elem;
	int		min;

	elem = dlist_ba->first;
	min = *((int*)elem->content);
	while (elem)
	{
		if (*((int*)elem->content) < min)
			min = *((int*)elem->content);
		if (elem == dlist_ba->last)
			break ;
		elem = elem->next;
	}
	return (min);
}

static void		ft_rotate_to_beginning(t_dlist_b **dlist_ba,
	t_dlist_b **dlist_bb, t_env *env)
{
	size_t	forward;
	size_t	backward;
	int		min;

	min = ft_get_min(*dlist_ba);
	forward = ft_count_forward(*dlist_ba, min);
	backward = ft_count_backward(*dlist_ba, min);
	while (forward > 0 && forward <= backward)
	{
		ft_apply_instructions("ra", env, dlist_ba, dlist_bb);
		forward--;
	}
	while (backward > 0 && backward < forward)
	{
		ft_apply_instructions("rra", env, dlist_ba, dlist_bb);
		backward--;
	}
}

int				ft_insert_in_a(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
	t_env *env)
{
	t_list	*count;
	size_t	min_pos;

	while ((*dlist_bb)->size > 0)
	{
		if (!(count = ft_count_all_instructions(*dlist_ba, *dlist_bb)))
			return (0);
		min_pos = ft_get_min_pos(count);
		ft_lstdel(&count, &free);
		ft_insert_elem(dlist_ba, dlist_bb, env, min_pos);
	}
	ft_rotate_to_beginning(dlist_ba, dlist_bb, env);
	return (1);
}
