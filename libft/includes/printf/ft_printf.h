/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 12:35:48 by mverdier          #+#    #+#             */
/*   Updated: 2017/02/11 16:27:43 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "ftprintf_params.h"
# include "ftprintf_search.h"
# include "ftprintf_size.h"
# include "ftprintf_fill.h"
# include "ftprintf_colors.h"

# include "../libft/ft_memory.h"
# include "../libft/ft_string.h"

# include <stdarg.h>
# include <stdlib.h>
# include <unistd.h>
# include <stdbool.h>
# include <wchar.h>

int		ft_printf(const char *format, ...);

int		ft_dprintf(const int fd, const char *format, ...);

#endif
