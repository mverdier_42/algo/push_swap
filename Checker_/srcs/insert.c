/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   insert.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 19:15:28 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/11 17:31:31 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static int	ft_duplicate_error(char *str, t_env *env, int **nb)
{
	if (env->v)
		ft_printf("Error : the argument [%s] is a duplicate !\n", str);
	free(*nb);
	return (0);
}

static int	ft_check_duplicates(t_dlist_b *dlist_b, int nb)
{
	t_dlist	*list;

	list = dlist_b->first;
	while (list && list != dlist_b->last)
	{
		if (nb == *((int*)(list->content)))
			return (0);
		list = list->next;
	}
	if (list && list == dlist_b->last)
	{
		if (nb == *((int*)(list->content)))
			return (0);
	}
	return (1);
}

static int	ft_check_insert(t_dlist_b **dlist_b, char *str, t_env *env)
{
	int		*nb;

	if ((nb = (int*)malloc(sizeof(int))) == NULL)
		return (0);
	*nb = ft_atoi(str);
	if (!ft_check_duplicates(*dlist_b, *nb))
		return (ft_duplicate_error(str, env, &nb));
	ft_dlst_push_back(dlist_b, ft_dlstnew(nb));
	return (1);
}

static void	ft_skip_options(t_dlist_b **args, t_dlist *last, t_env *env)
{
	char		*tmp;

	while ((tmp = (char*)(*args)->first->content)
			&& (!ft_strcmp(tmp, "-v") || !ft_strcmp(tmp, "-va")
				|| !ft_strcmp(tmp, "-vf") || !ft_strcmp(tmp, "-a")
				|| !ft_strcmp(tmp, "-av") || !ft_strcmp(tmp, "-af")
				|| !ft_strcmp(tmp, "-f") || !ft_strcmp(tmp, "-fa")
				|| !ft_strcmp(tmp, "-fv") || !ft_strcmp(tmp, "-vaf")
				|| !ft_strcmp(tmp, "-vfa") || !ft_strcmp(tmp, "-afv")
				|| !ft_strcmp(tmp, "-avf") || !ft_strcmp(tmp, "-fav")
				|| !ft_strcmp(tmp, "-fva")))
	{
		if ((*args)->first == last)
			break ;
		ft_rotate(args);
	}
	if (env->f)
		ft_rotate(args);
	if (env->a)
		ft_rotate(args);
}

int			ft_insert(t_dlist_b **dlist_b, t_dlist_b *args, t_env *env, int ret)
{
	t_dlist_b	*ins_args;
	t_dlist		*elem;
	t_dlist		*last;

	if (ret == 2)
		return (ret);
	last = args->last;
	ins_args = args;
	ft_skip_options(&ins_args, args->last, env);
	elem = ins_args->first;
	while (elem)
	{
		if (!ft_check_insert(dlist_b, elem->content, env))
			return (0);
		if (elem == last)
			break ;
		elem = elem->next;
	}
	return (1);
}
