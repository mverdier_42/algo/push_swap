/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rr.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 15:44:07 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/13 18:19:24 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_rr(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb)
{
	ft_rotate(dlist_ba);
	ft_rotate(dlist_bb);
}
