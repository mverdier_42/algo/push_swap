/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/10 21:31:42 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/11 15:40:07 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H

# include "libft.h"

# define MAX(x, y) x >= y ? x : y

typedef struct	s_env
{
	int			v;
	int			f;
	int			fd;
}				t_env;

void			*ft_init_lists(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
				t_dlist_b **args);
t_env			*ft_init_env(void);
int				ft_parse(t_dlist_b *args, t_env **env);
int				ft_get_options(t_dlist **args, t_dlist *last, t_env **env,
				size_t *n);
int				ft_get_fd(char *str, t_env **env);
int				ft_insert(t_dlist_b **dlist_b, t_dlist_b *args, t_env *env);

void			ft_push(t_dlist_b **src, t_dlist_b **des);
void			ft_swap(t_dlist_b **dlist_b);
void			ft_ss(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb);
void			ft_rotate(t_dlist_b **dlist_b);
void			ft_rr(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb);
void			ft_rrotate(t_dlist_b **dlist_b);
void			ft_rrr(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb);
void			ft_apply_instructions(char *instruction, t_env *env,
				t_dlist_b **dlist_ba, t_dlist_b **dlist_bb);

int				ft_sort_algo(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
				t_env *env);
void			ft_sort_three(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
				t_env *env);

int				ft_insert_in_a(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
				t_env *env);
size_t			ft_count_instructions(t_dlist_b *dlist_ba, t_dlist_b *dlist_bb,
				t_dlist *elem, size_t pos);
size_t			ft_count_forward_a(t_dlist_b *dlist_ba, int elem);
size_t			ft_count_backward_a(t_dlist_b *dlist_ba, int elem);
size_t			ft_count_forward(t_dlist_b *dlist_ba, int elem);
size_t			ft_count_backward(t_dlist_b *dlist_ba, int elem);
void			ft_insert_elem(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
				t_env *env, size_t min_pos);

int				ft_check_if_sorted(t_dlist_b *dlist_ba, t_dlist_b *dlist_bb);

void			ft_aff(t_dlist_b *dlist_ba, t_dlist_b *dlist_bb);
void			ft_delete_lists(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
				t_dlist_b **args, t_env **env);

#endif
