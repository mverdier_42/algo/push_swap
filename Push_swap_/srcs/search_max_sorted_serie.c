/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   search_max_sorted_serie.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/02 18:41:25 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/04 17:37:09 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	ft_compare_elems(t_dlist *elem, t_serie *serie, int start_tmp,
		int size)
{
	int		len;
	int		len_tmp;

	len = 1;
	len_tmp = len;
	while (elem)
	{
		if (*((int*)elem->content) < *((int*)elem->next->content))
			len++;
		else
		{
			if (len > len_tmp)
			{
				serie->start = start_tmp;
				serie->end = *((int*)elem->content);
				len_tmp = len;
			}
			else if (len == len_tmp && size <= 0)
				return ;
			len = 1;
			start_tmp = *((int*)elem->next->content);
		}
		elem = elem->next;
		size--;
	}
}

void		ft_search_max_sorted_serie(t_dlist_b *dlist_ba, t_serie *serie)
{
	t_dlist	*elem;
	int		start_tmp;

	elem = dlist_ba->first;
	serie->start = *((int*)elem->content);
	serie->end = serie->start;
	start_tmp = serie->start;
	ft_compare_elems(elem, serie, start_tmp, dlist_ba->size);
}
