/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_median.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/24 14:10:54 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/27 18:46:40 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		ft_get_five_biggers(t_dlist_b *dlist_b)
{
	size_t	i;
	int		*tab;
	int		pivot;
	t_dlist	*list;

	if ((tab = (int*)malloc(sizeof(int) * dlist_b->size)) == NULL)
		return (-1);
	list = dlist_b->first;
	i = 0;
	while (i < dlist_b->size)
	{
		tab[i] = *((int*)(list->content));
		list = list->next;
		i++;
	}
	ft_sortinttab(tab, dlist_b->size);
	pivot = tab[dlist_b->size - 6];
	free(tab);
	return (pivot);
}

int		ft_get_median(t_dlist_b *dlist_b, size_t len)
{
	size_t	i;
	int		*tab;
	int		pivot;
	t_dlist	*list;

	if ((tab = (int*)malloc(sizeof(int) * len)) == NULL)
		return (-1);
	list = dlist_b->first;
	i = 0;
	while (i < len)
	{
		tab[i] = *((int*)(list->content));
		list = list->next;
		i++;
	}
	ft_sortinttab(tab, len);
	pivot = tab[len / 2];
	free(tab);
	return (pivot);
}
