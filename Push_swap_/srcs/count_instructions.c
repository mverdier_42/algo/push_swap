/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   count_instructions.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/04 16:56:11 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/10 21:47:26 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

size_t	ft_count_left_side(t_dlist_b *dlist_bb, size_t forward,
	size_t backward, size_t pos)
{
	size_t	nbr;

	(void)dlist_bb;
	if (forward <= backward)
		nbr = MAX(pos, forward);
	else
		nbr = pos + backward;
	return (nbr + 1);
}

size_t	ft_count_right_side(t_dlist_b *dlist_bb, size_t forward,
	size_t backward, size_t pos)
{
	size_t	nbr;

	if (backward <= forward)
		nbr = MAX((dlist_bb->size - pos), backward);
	else
		nbr = (dlist_bb->size - pos) + forward;
	return (nbr + 1);
}

size_t	ft_count_instructions(t_dlist_b *dlist_ba, t_dlist_b *dlist_bb,
	t_dlist *elem, size_t pos)
{
	size_t	nbr;
	size_t	forward;
	size_t	backward;

	forward = ft_count_forward_a(dlist_ba, *((int*)elem->content));
	backward = ft_count_backward_a(dlist_ba, *((int*)elem->content));
	if (pos <= dlist_bb->size / 2)
		nbr = ft_count_left_side(dlist_bb, forward, backward, pos);
	else
		nbr = ft_count_right_side(dlist_bb, forward, backward, pos);
	return (nbr);
}
