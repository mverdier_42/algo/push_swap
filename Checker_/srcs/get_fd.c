/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_fd.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/17 16:15:45 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/11 15:31:37 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"
#include <fcntl.h>

int		ft_get_fd(char *str, t_env **env)
{
	int		fd;

	if ((fd = open(str, O_RDONLY)) < 0)
	{
		if ((*env)->v)
			ft_printf("Error : fail while trying to open the file [%s]\n", str);
		return (0);
	}
	(*env)->fd = fd;
	return (1);
}
