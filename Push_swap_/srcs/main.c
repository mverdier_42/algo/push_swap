/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/12 18:12:04 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/11 17:45:16 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	ft_error(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
		t_dlist_b **args, t_env **env)
{
	if (!(*env)->v)
		ft_printf("Error\n");
	ft_delete_lists(dlist_ba, dlist_bb, args, env);
	return (-1);
}

static void	ft_get_args(t_dlist_b **args, char **av)
{
	char	**tab;
	int		i;
	int		n;

	i = 1;
	while (av[i])
	{
		tab = ft_strsplit_str(av[i], " \t\n");
		n = 0;
		while (tab[n])
		{
			ft_dlst_push_back(args, ft_dlstnew(tab[n]));
			n++;
		}
		free(tab);
		i++;
	}
}

int			main(int ac, char **av)
{
	t_dlist_b	*dlist_ba;
	t_dlist_b	*dlist_bb;
	t_dlist_b	*args;
	t_env		*env;
	int			ret;

	(void)ac;
	if (!(env = ft_init_env()) || !ft_init_lists(&dlist_ba, &dlist_bb, &args))
		return (-1);
	ft_get_args(&args, av);
	if (!(ret = ft_parse(args, &env))
			|| !(ret = ft_insert(&dlist_ba, args, env)))
		return (ft_error(&dlist_ba, &dlist_bb, &args, &env));
	if (ret == 2)
	{
		ft_delete_lists(&dlist_ba, &dlist_bb, &args, &env);
		return (0);
	}
	if (dlist_ba->size < 4)
		ft_sort_three(&dlist_ba, &dlist_bb, env);
	else if (!ft_sort_algo(&dlist_ba, &dlist_bb, env))
		return (ft_error(&dlist_ba, &dlist_bb, &args, &env));
	ft_delete_lists(&dlist_ba, &dlist_bb, &args, &env);
	return (0);
}
