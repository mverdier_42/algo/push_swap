/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort_five.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/24 16:13:58 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/27 21:24:33 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	*ft_fill_tab(t_dlist_b *dlist_b)
{
	size_t	i;
	int		*tab;
	t_dlist	*elem;

	if ((tab = (int*)malloc(sizeof(int) * dlist_b->size)) == NULL)
		return (NULL);
	elem = dlist_b->first;
	i = 0;
	while (i < dlist_b->size)
	{
		tab[i] = *((int*)(elem->content));
		i++;
		elem = elem->next;
	}
	return (tab);
}

void		ft_sort_b_five(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
		t_env *env)
{
	int		*tab;
	size_t	size_tmp;

	if (!(tab = ft_fill_tab(*dlist_bb)) || (*dlist_bb)->size == 0)
		return ;
	size_tmp = (*dlist_bb)->size;
	ft_sortinttab(tab, size_tmp);
	while ((*dlist_bb)->size > 3)
	{
		if (*((int*)(*dlist_bb)->first->content) == tab[size_tmp - 1]
				|| (size_tmp == 5 && *((int*)(*dlist_bb)->first->content) ==
				tab[size_tmp - 2]))
			ft_apply_instructions("pa", env, dlist_ba, dlist_bb);
		else if (*((int*)(*dlist_bb)->first->content) ==
				tab[(*dlist_bb)->size - 1] &&
				ft_is_worth_to_rotate_sup(*dlist_bb, tab[2]))
			ft_apply_instructions("rb", env, dlist_ba, dlist_bb);
		else
			ft_apply_instructions("rrb", env, dlist_ba, dlist_bb);
	}
	if (size_tmp == 5 && *((int*)(*dlist_ba)->first->content) >
			*((int*)(*dlist_ba)->first->next->content))
		ft_apply_instructions("sa", env, dlist_ba, dlist_bb);
	ft_sort_b_three(dlist_ba, dlist_bb, env);
	while (size_tmp > 0)
	{
		ft_apply_instructions("ra", env, dlist_ba, dlist_bb);
		size_tmp--;
	}
	free(tab);
}
