/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aff.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/10 18:12:58 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/04 17:34:52 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static void	ft_aff_a(t_dlist_b *dlist_ba, t_dlist *lista)
{
	ft_printf("liste A : ");
	while (lista && lista != dlist_ba->last)
	{
		ft_printf("%d -> ", *((int*)(lista->content)));
		lista = lista->next;
	}
	if (lista && lista == dlist_ba->last)
		ft_printf("%d\n", *((int*)(lista->content)));
	else
		ft_printf("\n");
}

static void	ft_aff_b(t_dlist_b *dlist_bb, t_dlist *listb)
{
	ft_printf("liste B : ");
	while (listb && listb != dlist_bb->last)
	{
		ft_printf("%d -> ", *((int*)(listb->content)));
		listb = listb->next;
	}
	if (listb && listb == dlist_bb->last)
		ft_printf("%d\n", *((int*)(listb->content)));
	else
		ft_printf("\n");
}

void		ft_aff(t_dlist_b *dlist_ba, t_dlist_b *dlist_bb)
{
	t_dlist	*lista;
	t_dlist	*listb;

	if (!dlist_ba)
		lista = NULL;
	else
		lista = dlist_ba->first;
	if (!dlist_bb)
		listb = NULL;
	else
		listb = dlist_bb->first;
	ft_aff_a(dlist_ba, lista);
	ft_aff_b(dlist_bb, listb);
}
