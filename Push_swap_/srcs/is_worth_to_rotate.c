/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_worth_to_rotate.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/25 14:29:49 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/25 14:31:27 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int		ft_is_worth_to_rotate_sup(t_dlist_b *dlist, int pivot)
{
	int		next;
	int		prev;
	t_dlist	*elem;

	next = 0;
	prev = 0;
	elem = dlist->first;
	while (*((int*)(elem->content)) <= pivot)
	{
		next++;
		if (elem == dlist->last)
			break ;
		elem = elem->next;
	}
	elem = dlist->first;
	while (*((int*)(elem->content)) <= pivot)
	{
		prev++;
		if (elem->prev == dlist->first)
			break ;
		elem = elem->prev;
	}
	return (next <= prev);
}

int		ft_is_worth_to_rotate_inf(t_dlist_b *dlist, int pivot)
{
	int		next;
	int		prev;
	t_dlist	*elem;

	next = 0;
	prev = 0;
	elem = dlist->first;
	while (*((int*)(elem->content)) > pivot)
	{
		next++;
		if (elem == dlist->last)
			break ;
		elem = elem->next;
	}
	elem = dlist->first;
	while (*((int*)(elem->content)) > pivot)
	{
		prev++;
		if (elem->prev == dlist->first)
			break ;
		elem = elem->prev;
	}
	return (next <= prev);
}
