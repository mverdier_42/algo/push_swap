/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_lists.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/11 19:23:10 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/11 16:29:13 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	*ft_init_lists(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
		t_dlist_b **args)
{
	if ((*dlist_ba = ft_dlst_b_new()) == NULL)
		return (NULL);
	if ((*dlist_bb = ft_dlst_b_new()) == NULL)
		return (NULL);
	if ((*args = ft_dlst_b_new()) == NULL)
		return (NULL);
	return (*dlist_ba);
}
