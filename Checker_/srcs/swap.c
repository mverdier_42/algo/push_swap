/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/11 19:22:09 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/13 18:05:04 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void	ft_swap(t_dlist_b **dlist_b)
{
	t_dlist	*first;
	t_dlist	*second;

	if (!(*dlist_b)->first || (*dlist_b)->size == 1)
		return ;
	if ((*dlist_b)->size == 2)
	{
		ft_rotate(dlist_b);
		return ;
	}
	first = (*dlist_b)->first;
	second = first->next;
	first->next = second->next;
	first->next->prev = first;
	first->prev = second;
	second->next = first;
	second->prev = (*dlist_b)->last;
	(*dlist_b)->first = second;
	(*dlist_b)->last->next = (*dlist_b)->first;
}
