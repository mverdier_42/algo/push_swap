/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   aff.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/10 18:12:58 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/10 16:10:30 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static void	ft_aff_a(t_dlist_b *dlist_ba)
{
	t_dlist	*lista;

	lista = dlist_ba->first;
	ft_printf("liste A : ");
	while (lista && lista != dlist_ba->last)
	{
		ft_printf("%d -> ", *((int*)(lista->content)));
		lista = lista->next;
	}
	if (lista && lista == dlist_ba->last)
		ft_printf("%d\n", *((int*)(lista->content)));
	else
		ft_printf("\n");
}

static void	ft_aff_b(t_dlist_b *dlist_bb)
{
	t_dlist	*listb;

	listb = dlist_bb->first;
	ft_printf("liste B : ");
	while (listb && listb != dlist_bb->last)
	{
		ft_printf("%d -> ", *((int*)(listb->content)));
		listb = listb->next;
	}
	if (listb && listb == dlist_bb->last)
		ft_printf("%d\n", *((int*)(listb->content)));
	else
		ft_printf("\n");
}

void		ft_aff(t_dlist_b *dlist_ba, t_dlist_b *dlist_bb, char *line)
{
	int		i;

	i = 0;
	ft_printf("%c[2J%c[H", 27, 27);
	while (i < 40)
	{
		ft_printf("\n");
		i++;
	}
	ft_aff_a(dlist_ba);
	ft_aff_b(dlist_bb);
	if (line)
		ft_printf("last instruction : %s\n", line);
}
