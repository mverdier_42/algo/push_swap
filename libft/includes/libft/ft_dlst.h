/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dlst.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/07 13:55:17 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/11 18:13:21 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_DLST_H
# define FT_DLST_H

# include <stdlib.h>

typedef struct	s_dlist
{
	void			*content;
	struct s_dlist	*prev;
	struct s_dlist	*next;
}				t_dlist;

typedef struct	s_dlist_b
{
	t_dlist			*first;
	t_dlist			*last;
	size_t			size;
}				t_dlist_b;

t_dlist			*ft_dlstnew(void *content);
t_dlist_b		*ft_dlst_b_new(void);
void			ft_dlst_push_back(t_dlist_b **dlist_b, t_dlist *new);
void			ft_dlst_push_front(t_dlist_b **dlist_b, t_dlist *new);
void			ft_dlstdel(t_dlist_b **dlist_b, void (*del)(void*));
void			ft_dlstdelone(t_dlist **dlist, void (*del)(void*),
		t_dlist_b **dlist_b);

#endif
