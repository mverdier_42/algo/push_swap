/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <mverdier@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/09 15:39:44 by mverdier          #+#    #+#             */
/*   Updated: 2017/11/11 17:36:17 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static int	ft_error(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb, t_env **env,
		t_dlist_b **args)
{
	if (!(*env)->v)
		ft_printf("Error\n");
	ft_delete_lists(dlist_ba, dlist_bb, env, args);
	return (-1);
}

static int	ft_return_ko(t_dlist_b **dlist_ba, t_dlist_b **dlist_bb,
		t_env **env, t_dlist_b **args)
{
	if (!(*env)->v)
		ft_printf("KO\n");
	ft_delete_lists(dlist_ba, dlist_bb, env, args);
	return (-1);
}

static int	ft_get_args(t_dlist_b **args, int ac, char **av)
{
	char	**tab;
	int		i;
	int		n;

	i = 1;
	if (ac < 2)
		return (0);
	while (i < ac && av[i])
	{
		tab = ft_strsplit_str(av[i], " \t\n");
		n = 0;
		while (tab[n])
		{
			ft_dlst_push_back(args, ft_dlstnew(tab[n]));
			n++;
		}
		free(tab);
		i++;
	}
	return (1);
}

int			main(int ac, char **av)
{
	t_dlist_b	*dlist_ba;
	t_dlist_b	*dlist_bb;
	t_dlist_b	*args;
	t_env		*env;
	int			ret;

	if (!(env = ft_init_env()) || !ft_init_lists(&dlist_ba, &dlist_bb, &args))
		return (-1);
	if (!ft_get_args(&args, ac, av))
		return (0);
	if (!(ret = ft_parse(args, &env))
			|| !(ret = ft_insert(&dlist_ba, args, env, ret)))
		return (ft_error(&dlist_ba, &dlist_bb, &env, &args));
	if (ret == 2)
	{
		ft_delete_lists(&dlist_ba, &dlist_bb, &env, &args);
		return (0);
	}
	if (!ft_execute_instructions(&dlist_ba, &dlist_bb, env))
		return (ft_error(&dlist_ba, &dlist_bb, &env, &args));
	if (!ft_check_if_sorted(dlist_ba, dlist_bb, env))
		return (ft_return_ko(&dlist_ba, &dlist_bb, &env, &args));
	ft_printf("OK\n");
	ft_delete_lists(&dlist_ba, &dlist_bb, &env, &args);
	return (0);
}
