/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   apply_instructions.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mverdier <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/10/26 20:52:51 by mverdier          #+#    #+#             */
/*   Updated: 2017/10/26 22:27:40 by mverdier         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	ft_apply_instructions(char *instruction, t_env *env,
		t_dlist_b **dlist_ba, t_dlist_b **dlist_bb)
{
	ft_dprintf(env->fd, "%s\n", instruction);
	if (!strcmp(instruction, "pa"))
		ft_push(dlist_bb, dlist_ba);
	if (!strcmp(instruction, "pb"))
		ft_push(dlist_ba, dlist_bb);
	if (!strcmp(instruction, "sa") || !strcmp(instruction, "ss"))
		ft_swap(dlist_ba);
	if (!strcmp(instruction, "sb") || !strcmp(instruction, "ss"))
		ft_swap(dlist_bb);
	if (!strcmp(instruction, "ra") || !strcmp(instruction, "rr"))
		ft_rotate(dlist_ba);
	if (!strcmp(instruction, "rb") || !strcmp(instruction, "rr"))
		ft_rotate(dlist_bb);
	if (!strcmp(instruction, "rra") || !strcmp(instruction, "rrr"))
		ft_rrotate(dlist_ba);
	if (!strcmp(instruction, "rrb") || !strcmp(instruction, "rrr"))
		ft_rrotate(dlist_bb);
}
